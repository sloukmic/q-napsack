from tkinter import *



if __name__ == '__main__':
    #colors
    big_background = "#F4E1E3"
    app_background = "#FEC4DF"
    not_used_color = big_background
    used_color = "#A74A7B"
    default_font = ("monaco", 10, "bold")
    items_font = ("monaco", 10)


    root = Tk()
    root.geometry("1200x400")
    root.configure(background=big_background)

    top_frame = Frame(root, bg=app_background)
    top_frame.pack(side=TOP, padx=10, pady=10)

    left_top_frame = Frame(top_frame, bg=used_color, bd=3)
    left_top_frame.pack(side=LEFT, padx=10, pady=10)

    right_top_frame = Frame(top_frame, bg=not_used_color, bd=3)
    right_top_frame.pack(side=RIGHT, padx=10, pady=10)

    label = Label(left_top_frame, text="Included in optimization", padx=3, pady=3, bg=used_color, font=default_font)
    label.pack(pady=0)
    label2 = Label(right_top_frame, text="Available", padx=3, pady=3, bg=not_used_color, font=default_font)
    label2.pack(pady=0)
    #label2.grid(row=4, column=0)

    listbox_available = Listbox(left_top_frame, width=50, font=items_font, bg=used_color)

    listbox_available.insert(1, "Bitcoin        cost: $60,273.75  return: 60000")
    listbox_available.insert(2, "Dogecoin       cost: $0.1165     return: 9800")
    listbox_available.insert(3, "Tether USDt    cost: $0.9995     return: 7000")
    listbox_available.insert(4, "BNB            cost: $560.96     return: 600")
    listbox_available.insert(5, "Solana         cost: $128.87     return: 3000")
    listbox_available.insert(6, "Ethereum       cost: $3,295.81   return: 6000")
    listbox_available.insert(7, "Toncoin        cost: $7.33       return: 9800")
    listbox_available.insert(8, "XRP            cost: $0.4722     return: 7000")
    listbox_available.insert(9, "Shiba Inu      cost: $0.00001669 return: 600")
    listbox_available.insert(10, "Cardano        cost: $0.37       return: 3000")
    listbox_available.insert(11, "Chainlink      cost: 5000        return: 6000")
    listbox_available.insert(12, "USDC           cost: $0.9999     return: 9800")
    listbox_available.insert(13, "Avalanche      cost: $24.21      return: 7000")
    listbox_available.insert(14, "TRON           cost: $0.1193     return: 600")
    listbox_available.insert(15, "Polkadot       cost: $5.64       return: 3000")

    scrollbar = Scrollbar(left_top_frame)
    scrollbar.pack(side=RIGHT, fill=Y)
    listbox_available.configure(yscrollcommand=scrollbar.set)
    scrollbar.configure(command=listbox_available.yview)

    listbox_available.pack(side=RIGHT)

    listbox_selected = Listbox(right_top_frame, width=50, font=items_font, bg=not_used_color)
    listbox_selected.pack(side=LEFT)

    scrollbar2 = Scrollbar(right_top_frame)
    scrollbar2.pack(side=RIGHT, fill=Y)
    listbox_selected.configure(yscrollcommand=scrollbar.set)
    scrollbar2.configure(command=listbox_selected.yview)

    middle_frame = Frame(root, bg=big_background)
    middle_frame.pack(side=TOP, padx=10, pady=10)
    button1 = Button(middle_frame, text="Optimize", command=lambda: print('Optimized'), bg=app_background,
                     font=default_font, height=3, width=20)
    button1.pack(padx=5, pady=5)

    bottom_frame = Frame(root, bg=app_background)
    bottom_frame.pack(side=TOP, padx=10, pady=10)
    label3 = Label(bottom_frame, text="Optimized", padx=3, pady=3, bg=used_color, font=default_font)
    label3.pack(pady=0)

    listbox_chosen = Listbox(bottom_frame, width=50, font=items_font, bg=used_color)

    scrollbar2 = Scrollbar(bottom_frame)
    scrollbar2.pack(side=RIGHT, fill=Y)
    listbox_chosen.configure(yscrollcommand=scrollbar.set)
    scrollbar2.configure(command=listbox_chosen.yview)

    listbox_chosen.pack(side=TOP)

    def move_item_to_selected(event):
        selected_index = listbox_available.curselection()[0]
        listbox_selected.insert(1, listbox_available.get(selected_index))
        listbox_available.delete(selected_index)
        print("Removed item at index:", selected_index)

    # Bind the double-click event to the Listbox
    listbox_available.bind("<Double-Button-1>", move_item_to_selected)

    def move_item_to_available(event):
        selected_index = listbox_selected.curselection()[0]
        listbox_available.insert(1, listbox_selected.get(selected_index))
        listbox_selected.delete(selected_index)
        print("Removed item at index:", selected_index)

    # Bind the double-click event to the Listbox
    listbox_selected.bind("<Double-Button-1>", move_item_to_available)

    root.title("The great Knapsack Optimizer!!")
    root.mainloop()
