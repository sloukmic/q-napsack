from qiskit import ClassicalRegister, transpile
from qiskit.circuit.library import TwoLocal
from qiskit_aer import Aer
from qiskit.visualization import plot_distribution
from qiskit_algorithms.utils import algorithm_globals
from qiskit_algorithms import QAOA, NumPyMinimumEigensolver, VQE, SamplingVQE
from qiskit_algorithms.optimizers import COBYLA
from qiskit.primitives import Sampler, Estimator
from qiskit_optimization.algorithms import (
    MinimumEigenOptimizer,
    RecursiveMinimumEigenOptimizer,
    SolutionSample,
    OptimizationResultStatus,
)
from qiskit_optimization import QuadraticProgram
from qiskit.visualization import plot_histogram
from typing import List, Tuple
import numpy as np
import utils as ku


def get_quantum_knapsack_qubo(w, vals, capacity, penalty):
    qubo = QuadraticProgram()
    C = capacity
    P = penalty
    N = len(w)
    L = int(np.floor(np.log2(capacity)))+1

    for i in range(N):
        qubo.binary_var("x_" + str(i))
    for i in range(L):
        qubo.binary_var("y_" + str(i))

    A = [P * w[i] ** 2 - 2 * P * C * w[i] - vals[i] for i in range(N)]
    B = [P * (2**(2*b) - 2**(b+1)*C) for b in range(L)]

    Q = {}
    for i in range(N):
        for j in range(i + 1, N):
            k = 2 * P * w[i] * w[j]
            term = ("x_" + str(i), "x_" + str(j))
            Q[term] = k

    for b in range(L):
        for l in range(b+1, L):
            k = 2 * P * 2**(b+l)
            term = ("y_" + str(b), "y_" + str(l))
            Q[term] = k

    for b in range(L):
        for i in range(N):
            k = P * 2**(b+1) * w[i]
            term = ("x_" + str(i), "y_" + str(b))
            Q[term] = k

    qubo.minimize(linear=A+B, quadratic=Q)

    return qubo


def knapsack_qaoa(weights, values, capacity, penalty):
    qubo = get_quantum_knapsack_qubo(weights, values, capacity, penalty)

    qaoa_mes = QAOA(sampler=Sampler(), optimizer=COBYLA(), initial_point=[0.0, 0.0])

    qaoa = MinimumEigenOptimizer(qaoa_mes)  # using QAOA

    qaoa_result = qaoa.solve(qubo)
    return qaoa_result.x


def knapsack_vqe_solver(weights, values, capacity, penalty, reps=1, maxiter=50):
    N = len(weights)
    qubo = get_quantum_knapsack_qubo(weights, values, capacity, penalty)
    H, offset = qubo.to_ising()

    ansatz = TwoLocal(N, ['ry', 'rz'], 'cz', reps=reps, entanglement='linear')
    # print(ansatz.decompose().draw())

    vqe = SamplingVQE(sampler=Sampler(), ansatz=ansatz, optimizer=COBYLA(maxiter=maxiter))

    result = vqe.compute_minimum_eigenvalue(H)
    counts = result.eigenstate.binary_probabilities()
    return ku.prune_histogram(counts, len(weights))


def compare_solutions(weights, values, capacity, penalty):
    # Classic solution
    classic_value, dp = ku.knapsack_classic(weights, values, capacity)
    classic_selected_items = ku.get_selected_items_binary(weights, dp)

    # Quantum solution
    counts = knapsack_vqe_solver(weights, values, capacity, penalty)

    #print(vqe_solution)
    vqe_solution = max(counts, key=counts.get)
    vqe_solution_vector = [int(bit) for bit in vqe_solution[-len(weights):][::-1]]
    quantum_selected_items = [i for i, bit in enumerate(vqe_solution_vector) if bit == 1]
    quantum_value = sum(values[i] for i in quantum_selected_items)
    quantum_weight = sum(weights[i] for i in quantum_selected_items)

    while quantum_weight > capacity:
        min_val = np.inf
        min_id = -1
        for i in quantum_selected_items:
            if min_val > values[i]:
                min_id = i
                min_val = values[i]
        tmp = []
        for i in quantum_selected_items:
            if i != min_id:
                tmp.append(i)
        quantum_selected_items = tmp
        quantum_value -= min_val
        quantum_weight -= weights[min_id]
    value_ratio = 0
    if quantum_weight <= capacity:
        value_ratio = quantum_value / classic_value

    print('Values: ', values, "\nWeights: ", weights, '\nCapacity: ', capacity,
          '\nSelected items: ', quantum_selected_items, '\nOf value: ', quantum_value,
          '\nWith weight: ', quantum_weight,
          '\nFraction of optimal value: ', value_ratio)
    return value_ratio


if __name__ == "__main__":
    num_items = 5
    max_weight = 10
    max_value = 20
    capacity_ratio = 0.5
    its = 100

    acc = 0.0
    for _ in range(its):
        weights, values, capacity = ku.generate_knapsack_problem(num_items, max_weight, max_value, capacity_ratio)
        penalty = max(values) ** 2

        acc += compare_solutions(weights, values, capacity, penalty)
        #if succ: acc += 1
    print(f"succes ratio: {acc/its}")


