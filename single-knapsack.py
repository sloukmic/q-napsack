

# Integer knapasack problem

def print_table(dp):
    for row in dp:
        print(row)

def solve_knapsack(weights, values, capacity, multiple=False):
    n = len(weights)
    dp = [[0 for _ in range(capacity+1)] for _ in range(n+1)]
    for i in range(1, n+1):
        for j in range(1, capacity+1):
            if weights[i-1] <= j:
                if multiple:
                    dp[i][j] = max(dp[i-1][j], values[i-1] + dp[i][j-weights[i-1]])
                else:
                    dp[i][j] = max(dp[i-1][j], values[i-1] + dp[i-1][j-weights[i-1]])
            else:
                dp[i][j] = dp[i-1][j]
    #print_table(dp)
    return dp[n][capacity]

def generate_example(num_elements):
    import random
    weights = [random.randint(1, 300) for _ in range(num_elements)]
    values = [random.randint(1, 500) for _ in range(num_elements)]
    capacity = random.randint(20*num_elements, 100*num_elements)
    return weights, values, capacity


if __name__=="__main__":
    weights = [2, 3, 4, 5]
    values = [3, 4, 5, 6]
    capacity = 10
    #print(solve_knapsack(weights, values, capacity, multiple=True))
    print(solve_knapsack(weights, values, capacity, multiple=False))

    n = 500
'''
    ex = generate_example(n)

    print(ex)
    print(solve_knapsack(*ex, multiple=True))
    print(solve_knapsack(*ex, multiple=False))
'''