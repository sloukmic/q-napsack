from qiskit_optimization.applications import Knapsack
from qiskit_optimization.converters import QuadraticProgramToQubo

weights = [2, 3, 4, 5]
values = [3, 4, 5, 6]
W = 10  # Weight capacity

k = Knapsack(values,weights,W)

qp = k.to_quadratic_program()
print(qp.prettyprint())

conv = QuadraticProgramToQubo()
qubo = conv.convert(qp)
print(qubo.prettyprint())

op, offset = qubo.to_ising()
print(f"num qubits: {op.num_qubits}, offset: {offset}\n")
print(op)