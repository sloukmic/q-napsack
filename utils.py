def generate_knapsack_problem(num_items, max_weight, max_value, capacity_ratio=0.5):
    import random
    weights = [random.randint(1, max_weight) for _ in range(num_items)]
    values = [random.randint(1, max_value) for _ in range(num_items)]
    total_weight = sum(weights)
    capacity = int(total_weight * capacity_ratio)

    return weights, values, capacity

def knapsack_classic(weights, values, capacity):
    n = len(weights)
    dp = [[0 for x in range(capacity + 1)] for x in range(n + 1)]

    for i in range(n + 1):
        for w in range(capacity + 1):
            if i == 0 or w == 0:
                dp[i][w] = 0
            elif weights[i - 1] <= w:
                dp[i][w] = max(values[i - 1] + dp[i - 1][w - weights[i - 1]], dp[i - 1][w])
            else:
                dp[i][w] = dp[i - 1][w]

    return dp[n][capacity], dp

def get_selected_items(weights, dp):
    selected_items = []
    i = len(dp) - 1
    w = len(dp[0]) - 1

    while i > 0 and w > 0:
        if dp[i][w] != dp[i - 1][w]:
            selected_items.append(i - 1)
            w -= weights[i - 1]
        i -= 1

    selected_items.reverse()
    return selected_items

def get_selected_items_binary(weights, dp):
    selected_items = [0] * (len(dp) - 1)
    i = len(dp) - 1
    w = len(dp[0]) - 1

    while i > 0 and w > 0:
        if dp[i][w] != dp[i - 1][w]:
            selected_items[i - 1] = 1
            w -= weights[i - 1]
        i -= 1

    return selected_items


def prune_histogram(counts, N):
    result = {}
    for key, value in counts.items():
        new_key = key[-N:][::-1]
        result[new_key] = result.get(new_key, 0) + value
    return result
